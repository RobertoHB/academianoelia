<?php

namespace app\controllers;

use Yii;
use yii\helpers\ArrayHelper;
use app\models\Recibos;
use app\models\RecibosSearch;
use app\models\Matriculas;
use app\models\Alumnos;
use app\models\Clases;
use app\models\Cursos;
use app\models\Asignaturas;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use app\models\EmisionRecibos;
use \Mpdf\Mpdf;
//use app\models\EmisionRecibos;
//use yii\db\Connection;


//$connection = new Connection([
//    'dsn' => 'academia_noelia',
//   
//    'username' => 'root',
//    'password' => 'eldeorejo',
//]);
//$connection->open();
        




/**
 * RecibosController implements the CRUD actions for Recibos model.
 */
class RecibosController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Recibos models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RecibosSearch();
        $datos = new Recibos();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'datos' => $datos,
        ]);
    }

    /**
     * Displays a single Recibos model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Recibos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Recibos();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }
        
         //Consulta que devuelve los datos del alumno relacionados con la matricula
//         $datosAlumno = new SqlDataProvider([
//                             'sql' => "SELECT al.nombre nombre,al.apellidos apellidos, mat.id matri FROM matriculas mat JOIN 
//                                        alumnos al ON al.id = mat.alumno WHERE mat.id = $model->id"]);

        return $this->render('create', [
            'model' => $model,
//            'datosAlumno' => $datosAlumno,
        ]);
    }

    /**
     * Updates an existing Recibos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(Yii::$app->request->referrer);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }
    
     public function actionConsultar_emitidos()
    {
        $mensaje="";
        $model = new EmisionRecibos();
        return $this->render('consultarRecibos', [
            'model' => $model,
            'mensaje' => $mensaje,
        ]);
    }

     public function actionUpdate_emitidos($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(Yii::$app->request->referrer);
        }

        return $this->render('updateEmitidos', [
            'model' => $model,
        ]);
    }
    
    
    /**
     * Deletes an existing Recibos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

     public function actionEliminar_recibos($mes=NULL,$año=NULL)
    {
         
        echo ("El mes a eliminar es el : ".$mes." Y el año: ".$año); 
        exit;

        return $this->redirect(['index']);
    }
    
    
    public function actionRecibos_matricula($matricula){
        $searchModel = new RecibosSearch();
        //$datos = new Recibos();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere("matricula = '$matricula'");
        
       
        

        return $this->render('recibosMatricula', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
           
            //'datos' => $datos,
        ]);
  
    }
    
    
    /**
     * Finds the Recibos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Recibos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Recibos::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
   
    
    //Emision de recibos. Criterios de la consulta: Matriculas en activo, es decir, con fecha de baja vacia o nula. Se insertaran los valores de
     //Matricula-Emision(fecha en la que se emiten)-Mes(input)-Año(input)-Estado(0=pendiente)-Reducido(%=porcentaje de descuento)-Importe(valor recogido del Importe de la clase)
    
    public function actionEmitir_recibos($mensaje = null){
        $model = new EmisionRecibos();
        return $this->render('emision',['model'=>$model,'mensaje'=>$mensaje]);
        
    }
        public function actionConsultar_recibos($mensaje = null){
        $model = new EmisionRecibos();
        return $this->render('filtrado',['model'=>$model,'mensaje'=>$mensaje]);
        
    }
    
    
    public function actionEmitir($mesEmi = null,$anyoEmi = null){
    $model = new EmisionRecibos();
    $params = ['mesRecibo'=>$model->mesRecibo,'anyoRecibo'=>$model->anyoRecibo];
    $mensaje="";
     if ($model->load(Yii::$app->request->post())) {
            $mesEmitir = $model->mesRecibo;
            $anyoEmitir = $model->anyoRecibo;
            $fechaEmitir = $model->fechaEmision;
          
        if($model->validarEmision($model->mesRecibo,$model->anyoRecibo)) {
            $errors = $model->errors;
            $mensaje= "Recibos duplicados. No se emitirán.";
            return $this->redirect(["emitir_recibos", "mensaje" => $mensaje]);
            //return $this->redirect("Yii::$app->request->referrer ?: Yii::$app->homeUrl","mensaje"=>$mensaje);
            exit;
        }   
        
            //creamos la consulta con los registros que generan los recibos

                $dataProvider = new SqlDataProvider([
                    'sql' => "SELECT matriculas.id matricula,alumno,c1.id clase,c1.curso curso,c1.asignatura asignatura,c1.importe importe FROM matriculas JOIN (SELECT cl.id,cur.curso,a.asignatura,cl.importe fROM clases cl JOIN cursos cur ON cl.curso = cur.id
                             JOIN asignaturas a ON a.id = cl.asignatura)c1
                              ON clase = c1.id WHERE baja IS NULL or baja = 0 ORDER BY matricula",        
                ]);



            // bucle que va insertando los registros en recibos con los criterios de seleccion y los emitidos a traves del formulario

                $resultado = $dataProvider->getModels();


                foreach ($resultado as $value) {
                        $modelx = new Recibos();
                        $modelx->matricula = $value['matricula'];
                        $modelx->emision = $fechaEmitir;
                        $modelx->mes =  $mesEmitir;
                        $modelx->año = $anyoEmitir;
                        $modelx->estado = 0;
                        $modelx->reducido = "";
                        $modelx->importe = $value['importe'];                  
                        $modelx->save();
                       }
                       
                 return $this->redirect(["consulta_emitidos", "mensaje" => $mensaje,"mesEmi" => $mesEmitir,"anyoEmi" => $anyoEmitir]);       
              
       
        
     }         
} 
    //  controlamos si es una emision nueva o una consulta de recibos emitidos                 
    public function actionConsulta_emitidos($mesEmi = null,$anyoEmi = null){    
       
//       var_dump($_POST);
//        exit;
         $model = new EmisionRecibos();
         
     
         
        if(!isset($mesEmi)){
            $mesEmitir = $_POST['EmisionRecibos']['mesRecibo'];
            $anyoEmitir = $_POST['EmisionRecibos']['anyoRecibo'];
        }else{
            $mesEmitir = $_GET["mesEmi"];
            $anyoEmitir = $_GET["anyoEmi"];
        }
       
        $parametros=['mes'=>$mesEmitir,"anyo"=>$anyoEmitir];
       
        
                $RecibosEmitidos = new SqlDataProvider([
                     'sql' => "SELECT r.id id, r.matricula matricula,r.emision emision,r.mes mes,r.año año,r.estado estado,r.reducido reducido,r.importe importe,m.alumno alumno, m.clase clase
                                FROM recibos r join matriculas m on r.matricula = m.id WHERE r.mes = '$mesEmitir' and r.año = '$anyoEmitir'",
                ]);

              // $resultados = ArrayHelper::toArray($dataRecibosEmitidos);               

    //                   $RecibosEmitidos = ArrayHelper::toArray(Recibos::find()
    //                         ->select('matricula,emision,mes,año,estado,reducido,importe')
    //                            ->where(['mes' => $model->mesRecibo])
    //                           ->andWhere("año = $model->anyoRecibo")->all());
    //                   echo"<pre>";
    //                   var_dump($RecibosEmitidos);
    //                   echo"</pre>";

    ////                        
                       return $this->render('emitidos',[
                        'model'=>$model,
                        "datos"=>$RecibosEmitidos,
                        "parametros" => $parametros,
                        "campos"=>['id','matricula','emision','mes','año','estado','reducido','importe','alumno','clase'],
                        "titulo"=>"Emisión de Recibos / Matricula",
                        "enunciado"=>"Matriculas a emitir",
                        "sql"=>"SELECT count(*) FROM recibos r join matriculas m on r.matricula = m.id WHERE r.mes = '$mesEmitir' and r.año = '$anyoEmitir'",
                    ]);

}   
         
    public function actionFiltrar_recibos(){
         $model = new EmisionRecibos();
         //$modelRecibos = new recibos;
         if ($model->load(Yii::$app->request->post())) {
            $mesEmitir = $model->mesRecibo;
            $anyoEmitir = $model->anyoRecibo;
            $fechaEmitir = $model->fechaEmision;
            $RecibosFiltrados = new SqlDataProvider([
                'sql' => "SELECT r.id id, r.matricula matricula,r.emision emision,r.mes mes,r.año año,r.estado estado,r.reducido reducido,r.importe importe,m.alumno alumno, m.clase clase
                        FROM recibos r join matriculas m on r.matricula = m.id WHERE (r.mes = '$mesEmitir' and r.año = '$anyoEmitir') or emision = '$fechaEmitir'",
            ]);
            return $this->render('emitidos',[
                                'model'=>$model,
                                "datos"=>$RecibosFiltrados,
                                "campos"=>['id','matricula','emision','mes','año','estado','reducido','importe','alumno','clase'],
                                "titulo"=>"Consulta de Recibos / Matricula",
                                "enunciado"=>"Recibos a consultar",
                                "sql"=>"SELECT count(*) FROM recibos r join matriculas m on r.matricula = m.id WHERE r.mes = '$mesEmitir' and r.año = '$anyoEmitir'",
                            ]);
            
         }
        
    }
        
    public function actionRecibospdf(){
//        $Recibospdf = new SqlDataProvider([
//            'sql' => "SELECT r.id recibo, r.matricula matricula,r.emision emision,r.mes mes,r.año año,r.estado estado,r.reducido reducido,r.importe importe,m.alumno alumno, m.clase clase
//                       FROM recibos r join matriculas m on r.matricula = m.id WHERE r.mes = 12 and r.año = 2019",
//        ]);
        
         $model = new EmisionRecibos();
         $modelRecibos = new recibos;
         if ($model->load(Yii::$app->request->post())) {
            $mesEmitir = $model->mesRecibo;
            $anyoEmitir = $model->anyoRecibo;
            $fechaEmitir = $model->fechaEmision;
//            $RecibosFiltrados = new SqlDataProvider([
//                'sql' => " SELECT r.id recibo, r.matricula matricula,r.emision emision,r.mes mes,r.año año,r.estado estado,r.reducido reducido,r.importe importe,m.alumno alumno, m.clase clase,al.nombre nombre,al.apellidos apellidos,al.direccion direccion,al.dni dni,al.movil movil,cur.curso curso,asigna.asignatura asignatura
//                       FROM recibos r join matriculas m on r.matricula = m.id JOIN alumnos al ON m.alumno = al.id  JOIN clases cla ON cla.id = m.clase  JOIN cursos cur ON cur.id = cla.curso JOIN asignaturas asigna ON asigna.id = cla.asignatura
//                       WHERE (r.mes = '$mesEmitir' and r.año = '$anyoEmitir') or emision = '$fechaEmitir'",
////                'sql' => "SELECT r.id recibo, r.matricula matricula,r.emision emision,r.mes mes,r.año año,r.estado estado,r.reducido reducido,r.importe importe,m.alumno alumno, m.clase clase
////                        FROM recibos r join matriculas m on r.matricula = m.id WHERE (r.mes = '$mesEmitir' and r.año = '$anyoEmitir') or emision = '$fechaEmitir'",
//            ]);
            
            $alumnos_matricula_activa = new SqlDataProvider([
               'sql'=>"SELECT DISTINCT al.id alumno,al.nombre nombre,al.apellidos apellidos,al.dni dni,al.direccion direccion,al.poblacion poblacion,al.movil movil,centro.nombre centro
                                FROM alumnos al 
                                    JOIN centros centro on al.centro = centro.id
                                    JOIN matriculas mat ON al.id = mat.alumno 
                                    JOIN recibos rec 
                                        WHERE mat.baja IS NULL 
                                            AND mat.alumno 
                                                NOT IN(SELECT alumno_grupo FROM agrupan) 
                                                    AND rec.mes = $mesEmitir
                                                     AND rec.año = $anyoEmitir", 
            ]);
            
            
            
            $recibos_alumno = new SqlDataProvider([
                'sql'=>"SELECT r.id recibo, mat.id matricula,r.emision fechemi,r.mes mes,r.año año,r.importe importe,mat.alumno, cur.curso curso, asigna.asignatura asignatura 
                                    FROM recibos r JOIN matriculas mat ON mat.id = r.matricula
                                        JOIN clases clas ON mat.clase = clas.id 
                                        JOIN cursos cur ON clas.curso = cur.id 
                                        JOIN asignaturas asigna ON clas.asignatura = asigna.id 
                                            WHERE baja IS NULL
                                                AND r.mes = $mesEmitir
                                                AND r.año = $anyoEmitir 
                                                    ORDER BY recibo",
            ]);
            
            $recibos_alumnoAgrupado= new SqlDataProvider([
              'sql'=>"SELECT r.id recibo, mat.id matricula,r.emision fechemi,r.mes mes,r.año año,r.importe importe,agrup.alumno alumno, cur.curso curso, asigna.asignatura asignatura 
                            FROM recibos r 
                                JOIN matriculas mat ON mat.id = r.matricula
                                JOIN clases clas ON mat.clase = clas.id JOIN cursos cur ON clas.curso = cur.id JOIN asignaturas asigna ON clas.asignatura = asigna.id
                                JOIN agrupan agrup ON mat.alumno = agrup.alumno_grupo
                                    WHERE baja IS NULL
                                        AND r.mes = $mesEmitir
                                        AND r.año = $anyoEmitir 
                                            ORDER BY recibo", 
            ]);
            
            $recibos_alumnoAgrupadoMaterial = new SqlDataProvider([
                'sql'=>"SELECT a.id alumno,SUM(importe)totalmaterialAgr FROM alumnos a RIGHT JOIN agrupan ON alumno = a.id 
                        JOIN material ON (a.id = material.alumno 
                                  OR alumno_grupo = material.alumno)
                                    WHERE MONTH(fecha) = $mesEmitir 
                                            AND year(fecha) = $anyoEmitir 
                                             GROUP BY a.id",
            ]);
            
             $recibos_alumnoNoAgrupadoMaterial = new SqlDataProvider([
                'sql'=>"SELECT m.alumno alumno, SUM(importe)totalmaterialNoagr FROM material m
                        WHERE MONTH(fecha) = $mesEmitir 
                            AND year(fecha) = $anyoEmitir 
                            AND m.alumno NOT IN(SELECT DISTINCT alumno FROM agrupan)
                            AND m.alumno NOT IN(SELECT DISTINCT alumno_grupo FROM agrupan)
                              GROUP BY m.alumno",
            ]);
            
            $recibos_alumnoPendientes =  new SqlDataProvider([
                'sql'=>"SELECT mat.alumno alumno,rec.id recibo, rec.mes mes, rec.año, rec.emision, rec.importe
                         FROM recibos rec 
                            JOIN matriculas mat ON mat.id = rec.matricula 
                                WHERE rec.estado = FALSE 
                                and rec.mes <> $mesEmitir
                                and rec.año <= $anyoEmitir",
            ]);
            
        
        $resultado = $alumnos_matricula_activa->getModels();
        $recibos_activos = $recibos_alumno->getModels();
        $recibos_agrupados = $recibos_alumnoAgrupado->getModels();
        $recibos_material_agrupado = $recibos_alumnoAgrupadoMaterial->getModels();
        $recibos_material_noagrupado = $recibos_alumnoNoAgrupadoMaterial->getModels();
        $recibos_pendientes = $recibos_alumnoPendientes->getModels();
       
          
       
                $mpdf = NEW Mpdf;
             
                
                foreach ($resultado as $value) {
                    $total = 0;
                     $swAlumnoMaterial = 0; //switch que controla si un alumno tiene material de fotocopias o no
                    
                $reciboPlantilla= '<!DOCTYPE html>
                    <html>
                    <head>
                        <meta charset="UTF-8">
                        <meta name="viewpoort" content="width=device-width,initial-scale=1.0">
                    </head>
                    <style>
                        main{
                            width:2400px;
                            height:100px;
                        }
                        header{
                            float:left;
                            margin-left:20px;
                            margin-top:10px;
                          
                            width:95%;
                            height:25%;   
                        }
                        header .cab1,header .cab2,header .cab3{
                            width: 30%;
                            height: 50px;
                           
                            float:left;
                            margin-left:22px;
                            margin-top:2px;
                        }
                        header .cab1{
                            background-image: url("../data/imagenes/LogoAcademiaNoelia.jpg");
                            background-repeat: no-repeat;
                            background-size: 80%;  
                        }
                        header .cab2{
                            width:25%;
                            font-size: 8px;
                            text-align: center;
                            position:relative;
                            top:10px;
                        }   
                        header .cab3{
                            width: 32%;
                            font-size: 15px;
                            text-align: right;
                            line-height: 150px;
                        }  
                        section{
                           
                            margin-left:20px;
                            margin-top:1px;
                            width:95%;
                          
                        }
                      
                        section.datos_observa{
                            margin-bottom:10px;
                        }
                        div.personales{
                            float:left;
                            width:60%;
                            padding:10px;
                            margin-top:-170px;
                            border:1px solid #220044;
                            background-color: #f0f2ff;
                            border-style:solid;
                            border-radius: 15px;
          
                        }
                        div.pago{
                            float:left;
                            width:30%;
                            height:30px;
                            margin-left:10px;
                            padding:10px;
                            border:1px solid #220044;
                            background-color: #f0f2ff;
                            border-style:solid;
                            border-radius: 15px;

                        }
                        div.personales label, div.pago label{
                            background-color:#C2CCD1;
                        }

                        table th{
                            background-color: #C2CCD1;
                            text-align: left;
                            padding:2px;
                        }
                        table#tbl_datos{
                            width:700px;
                           
                        }
                        table#tbl_pago{
                            width:330px;
                            border:1px black solid;
                            margin-left:15px;
                            height:76px;
                            text-align: center;
                            border-radius: 10px;
                        }
                        table#tbl_pago th{
                            text-align: center;
                        }
                         div.concepto{
                            border:1px solid #220044;
                            background-color: #f0f2ff;
                            border-radius: 15px;
                           
                         }
                         table#tbl_concepto{
                            width:935px;
                            border:1px black solid;
                            text-align: center;
                            margin-left:10px;
                            border-radius: 10px;
                          
                        }
                        table#tbl_concepto th{
                            text-align: center; 
                            
                        }
                        div.total{
                            border:1px solid #220044;
                            background-color: #f0f2ff;
                            border-radius: 15px;
                        
                        }
                        table#tbl_observaciones{
                            width:935px;
                            border:1px black solid;
                            text-align: center;
                            margin-left:10px;
                            border-radius: 10px;
                          
                        }
                        table#tbl_observaciones th{
                            text-align: center; 
                            
                        }
                         div.observaciones{
                            border:1px solid #220044;
                            background-color: #f0f2ff;
                            border-radius: 15px;
                            text-align:left;
                        }
                        table#tbl_totales{
                            width:935px;
                            border:1px black solid;
                            text-align: center;
                            margin-left:10px;
                            border-radius: 10px;
                            
                        }
                        table#tbl_totales th{
                            text-align: center;
                            width:720px;
                        }
                        .contenedor_central{
                            margin-top:-100px;
                        }
                        @media print {
                            footer {
                                page-break-after: always;
                            }
                        }
                        

                    </style>
                    <body>
                        <main>     

                            <header>
                                <div class="cab1">

                                </div>
                                <div class="cab2">
                                    <strong>CENTRO DE ESTUDIOS NOELIA PÉREZ</strong><br/>
                                    <span>Residencial Monte Castillo</span>
                                    <span>Avda. de Oviedo, 20 E-F bajos</span><br/>
                                    <span>39710 SOLARES (Cantabria)</span><br/>
                                    <span>Telf.: 619 518 491</span><br/>
                                    <span>academianoeliaperez@gmail.com</span>
                                </div>
                                
                            </header>
                            <section class="datos_alumno">
                                <div class="personales">
                                        <table id="tbl_datos">
                                            <tr colspan="2">
                                                <th>Recibimos de</th>
                                                <td>'.$value['nombre'].' '.$value['apellidos'].'</td>   
                                            </tr>
                                            <tr colspan="2">
                                                <th>Centro</th><td>'.$value['centro'].'</td>
                                                <th>Telefono</th><td>'.$value['movil'].'</td>
                                            </tr>    
                                            <tr colspan="2">
                                                <th>Dirección</th><td>'.$value['direccion'].'</td>
                                                 <th>Ciudad</th><td>'.$value['poblacion'].'</td>
                                            </tr>


                                        </table>
                                </div>
                                <div class="pago">
                                    <table id="tbl_pago">
                                            <tr colspan="4">
                                                <th>Fecha</th>
                                                <th>Forma de Pago</th>
                                                <th>Período</th>

                                            </tr>
                                            <tr colspan="4">
                                                <td>'.$fechaEmitir.'</td>
                                                <td>Efectivo</td>
                                                <td>'.$mesEmitir.'/'.$anyoEmitir.'</td>

                                            </tr> 
                                    </table>
                                </div>
                            </section>
                         <div class="contenedor_central">   
                            <section class="datos_clase">
                              <div class="concepto">
                                <table id="tbl_concepto">
                                    <tr colspan="2">
                                        <th>Recibo</th>
                                        <th>Concepto</th>
                                        <th>Valor</th>
                                    </tr>';
                               
                                    foreach ($recibos_activos as $activos) {
                                        if($value['alumno'] == $activos['alumno'] ){
                                         $reciboPlantilla .= '<tr colspan="2">
                                                <td>'.$activos['recibo'].'</td>
                                                <td>'.$activos['curso']. ' - '.$activos['asignatura'].'</td>
                                                <td></td>
                                            </tr>'; 
                                         $total += $activos['importe'];
                                        }
                                     }
                                    foreach ($recibos_agrupados as $agrupados) {
                                        if($value['alumno'] == $agrupados['alumno'] ){
                                         $reciboPlantilla .= '<tr colspan="2">
                                                <td>'.$agrupados['recibo'].'</td>
                                                <td>'.$agrupados['curso']. ' - '.$agrupados['asignatura'].'</td>
                                                <td></td>
                                            </tr>';   
                                          $total += $agrupados['importe'];
                                        }
                                     }
                                     foreach ($recibos_material_agrupado as $material) {
                                        if($value['alumno'] == $material['alumno'] ){
                                         $reciboPlantilla .= '<tr colspan="2">
                                                <td>material</td>
                                                <td>Fotocopias</td>
                                                <td>'.$material['totalmaterialAgr'].'</td>
                                            </tr>';  
                                        $total += $material['totalmaterialAgr'];
                                        $swAlumnoMaterial = 1;
                                        }
                                     }
                                    foreach ($recibos_material_noagrupado as $materialNoAgr) {
                                        if($value['alumno'] == $materialNoAgr['alumno'] ){
                                         $reciboPlantilla .= '<tr colspan="2">
                                                <td>material</td>
                                                <td>Fotocopias</td>
                                                <td>'.$materialNoAgr['totalmaterialNoagr'].'</td>
                                            </tr>';  
                                        $total += $materialNoAgr['totalmaterialNoagr'];
                                         $swAlumnoMaterial = 1;
                                        }
                                     }
                                     
                              
                                     
                                     if($swAlumnoMaterial == 0){
                                        $reciboPlantilla .= '<tr colspan="2">
                                            <td>material</td>
                                            <td>Fotocopias</td>
                                            <td>0</td>
                                        </tr>';   
                                         //$swAlumnoMaterial = 0; //switch que controla si un alumno tiene material de fotocopias o no
                                     }
                                    
                            $reciboPlantilla.= '</table>
                              </div>
                            </section>
                            <section class="totales">
                                <div class="total">
                                    <table id="tbl_totales">
                                        <tr colspan="2">
                                            <th>Total</th><td>'.number_format($total, 2, '.', '').'</td>
                                        </tr>
                                    </table>
                                </div>
                            </section>
                            <section class="datos_observa">
                              <div class="observaciones">
                                <table id="tbl_observaciones">
                                   
                                
                                    <tr colspan="1">
                                        <th>Observaciones</th>
                                    </tr>
                                    <tr colspan="3">
                                        <td>Recibos Pendientes:</td>';
                                        foreach ($recibos_pendientes as $pendientes) {
                                           if($value['alumno'] == $pendientes['alumno'] ){
                                            $reciboPlantilla .=                                            
                                                   '<td>'.$pendientes['recibo'].'/'.$pendientes['mes'].'-'.$pendientes['año'].'/'.$pendientes['importe'].'</td>
                                               </tr>';  
       
                                           }
                                        };     
                                            
         
            $reciboPlantilla .= '</table>
                              </div>
                            </section>
                         
                            
                   </footer>
                        </main> 
                        <hr>  
                    </body>
                    </div>
                    </html>';
//                    
//                    $reciboPlantilla = '<div class="recibos-index">
//                                    <section class="cabecera" style="width:700px;height:100px">  
//                                        <div class="logo" style="float:left;width:33%">
//                                            <img src="../data/imagenes/logo.png"/>  
//                                        </div>
//        
//       
//                                        <div class="datosAca" style="float:left;width:33%;text-align:center;line-height:1px;font-size:10px;font-famili:arial;">
//                                            <p><b>Academia Noelia Pérez</b></p>
//                                            <p>Avenida de Oviedo, 20 bajos</p>
//                                            <p>Solares</p>
//                                            <p>(+34) 619 51 84 91</p>
//                                            <p>academianoeliaperez@gmail.com</p>
//                                            
//                                        </div>
//                                        <div class="recibonum" style="float:left;width:33%;padding-top:100px;text-align:right">
//                                            <span>Recibo Nº'.$value['recibo'].'</span>
//                                        </div>
//                                    </section>
//                                    <section class="cabecera1" style="width:700px;height:75px;float:left;clear:both">
//                                        <div style="float:left;width:400px;">
//                                            <table style="border:solid 1px #b3b7bb;border-radius:35px;widht:100%;">
//                                                <thead style="float:left">
//                                                  <tr>
//                                                    <th style="display:block;background-color:#b3b7bb;font-size:12px">Recibimos de</th>
//                                                    <td style="float:right;font-size:12px;">'.$value['apellidos'].' ' .$value['nombre'].'</td>
//                                                  </tr>
//                                                  <tr>
//                                                    <th style="display:block;background-color:#b3b7bb;font-size:12px">Dirección</th>
//                                                    <td style="float:right;font-size:12px;">'.$value['direccion'].'</td>
//                                                  </tr>
//                                                </thead>
//                                               
//                                            </table>
//                                        </div>
//                                        <div style="float:right;width:200px;">
//                                            <table style="border:solid 1px #b3b7bb;border-radius:35px;widht:100%;">
//                                                <thead style="float:left">
//                                                  <tr>
//                                                    <th style="background-color:#b3b7bb;font-size:12px">Fecha Recibo</th><th style="background-color:#b3b7bb;font-size:12px">Medio de Pago</th>
//                                                  </tr>
//                                                  <tr>
//                                                        <td style="float:right;font-size:12px">'.$value['emision'].'</td>
//                                                        <td style="float:right;font-size:12px">Efectivo</td>
//                                                  </tr>
//                                            </table>
//                                        </div>
//                                        <div>
//                                        </div>
//                                        
//                                    </section>
//                                    
//                                    <section class="cabecera2" style="width:1000px;height:30px;float:left;clear:both">
//                                        <div style="float:left;width:700px;">
//                                            <table width="1000px" height="200px" style="border:solid 1px grey;border-radius:35px;">
//                                                <thead style="float:left">
//                                                  <tr>
//                                                    <th style="display:block;background-color:#b3b7bb;font-size:15px;display:block;width:200px">El valor de</th>
//                                                    <td style="float:left;font-size:12px;width:800px;">'.$value['importe'].'</td>
//                                                  </tr>
//                                                </thead>   
//                                            </table>
//                                        </div>    
//                                    </section>
//                                    <section class="cabecera3" style="width:1000px;height:70px;float:left;clear:both">
//                                        <div style="float:left;width:1000px;">
//                                            <table width="1000px" height="300px" style="border:solid 1px grey;border-radius:35px;">
//                                                <thead style="float:left">
//                                                    <tr>
//                                                        <th style="background-color:#b3b7bb;font-size:15px">Concepto</th>
//                                                    </tr>   
//                                                    <tr>
//                                                        <td style="font-size:12px;">'.$value['curso'].' / '.$value['asignatura'].'</td>
//                                                    </tr>
//                                                </thead>   
//                                            </table>
//                                        </div>    
//                                    </section>
//
//                                            
//                                </div>';
                    
//                    $recibo = "<div class='container'>
//                                <table style='border:solid;width:800px;'>
//                                    <tr>
//                                        <th>RECIBO Nº</th> <th>LOCALIDAD DE EXPEDICIÓN</th> <th>IMPORTE</th>
//                                    </tr>
//                                    <tr>
//                                        <td style='text-align:center;'>".$value['recibo']."<td/> <td style='text-align:center;'>Solares</td><td style='text-align:center;'>".$value['importe']."</td>
//                                    </tr>
//                                    <tr>
//                                     <th>FECHA DE EXPEDICIÓN</th> <th>VENCIMIENTO</th>
//                                    </tr>
//                                    <tr>
//                                        <td style='text-align:center;'>".$value['emision']."<td/><td style='text-align:center;'>01-01-2020</td>
//                                    </tr>
//                                </table>   
//                            </div><hr>";
                       $mpdf->keep_table_proportions = true;
                       $mpdf->shrink_tables_to_fit = 1;
                       $mpdf->use_kwt = true; 
                       $mpdf->WriteHTML($reciboPlantilla); 
                    }
 
      
       
        $mpdf->Output();
        exit;
    }    
    }
         
         
         
    }
 
