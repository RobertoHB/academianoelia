<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Profesores;

/**
 * ProfesoresSearch represents the model behind the search form of `app\models\Profesores`.
 */
class ProfesoresSearch extends Profesores
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'movil', 'fijo'], 'integer'],
            [['nombre', 'apellidos', 'direccion', 'poblacion', 'dni', 'email', 'alta', 'baja', 'observaciones'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Profesores::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'movil' => $this->movil,
            'fijo' => $this->fijo,
            'alta' => $this->alta,
            'baja' => $this->baja,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'apellidos', $this->apellidos])
            ->andFilterWhere(['like', 'direccion', $this->direccion])
            ->andFilterWhere(['like', 'poblacion', $this->poblacion])
            ->andFilterWhere(['like', 'dni', $this->dni])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'observaciones', $this->observaciones]);

        return $dataProvider;
    }
}
