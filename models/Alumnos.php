<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "alumnos".
 *
 * @property int $id
 * @property string $nombre
 * @property string $apellidos
 * @property string $dni
 * @property string $direccion
 * @property string $poblacion
 * @property int $movil
 * @property int $fijo
 * @property string $email
 * @property string $alta
 * @property string $curso
 * @property string $centro
 * @property string $observaciones
 *
 * @property Agrupan[] $agrupans
 * @property CuentasBancarias[] $cuentasBancarias
 * @property Matriculas[] $matriculas
 */
class Alumnos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'alumnos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['movil', 'fijo'], 'integer'],
            [['alta'], 'safe'],
            //[['alta'], 'date', 'format' => 'php:Y-m-d'],
            [['observaciones'], 'string'],
            [['nombre', 'poblacion', 'centro'], 'string', 'max' => 100],
            [['apellidos', 'direccion', 'email'], 'string', 'max' => 200],
            [['dni'], 'string', 'max' => 9],
            [['curso'], 'string', 'max' => 50],
            [['pago'], 'string','max' => 50],
            [['dni'], 'unique']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'dni' => 'Dni',
            'direccion' => 'Direccion',
            'poblacion' => 'Poblacion',
            'movil' => 'Movil',
            'fijo' => 'Fijo',
            'email' => 'Email',
            'alta' => 'Alta',
            'curso' => 'Curso',
            'centro' => 'Centro',
            'pago' => 'Pago',
            'observaciones' => 'Observaciones',
        ];  
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgrupans()
    {
        return $this->hasMany(Agrupan::className(), ['alumno' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCuentasBancarias()
    {
        return $this->hasMany(CuentasBancarias::className(), ['alumno' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMatriculas(){
        return $this->hasMany(Matriculas::className(), ['alumno' => 'id']);
    }
    
    public function afterFind() {
        parent::afterFind();
        $this->alta=Yii::$app->formatter->asDate($this->alta, 'php:d-m-Y');;
        
    }

    
    public function beforeSave($insert) {
          parent::beforeSave($insert);
          $this->alta=Yii::$app->formatter->asDate($this->alta, 'php:Y-m-d');
          //$this->alta= \DateTime::createFromFormat("d/m/Y", $this->alta)->format("Y/m/d");
          return true;
    }

}
