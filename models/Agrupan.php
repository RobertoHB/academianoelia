<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "agrupan".
 *
 * @property int $id
 * @property int $alumno
 * @property int $alumno_grupo
 * @property string $observaciones
 *
 * @property Alumnos $alumno0
 */
class Agrupan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'agrupan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['alumno', 'alumno_grupo'], 'integer'],
            [['observaciones'], 'string'],
            [['alumno', 'alumno_grupo'], 'unique', 'targetAttribute' => ['alumno', 'alumno_grupo']],
            [['alumno'], 'exist', 'skipOnError' => true, 'targetClass' => Alumnos::className(), 'targetAttribute' => ['alumno' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'alumno' => 'Alumno',
            'alumno_grupo' => 'Alumno Grupo',
            'observaciones' => 'Observaciones',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlumno0()
    {
        return $this->hasOne(Alumnos::className(), ['id' => 'alumno']);
    }
        public function getAlumnoGrupo0()
    {
        return $this->hasOne(Alumnos::className(), ['id' => 'alumno_grupo']);
    }
}
