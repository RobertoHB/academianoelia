<?php

namespace app\models;
use yii\data\SqlDataProvider;
use app\models\Clases;
use app\models\Matriculas;

use Yii;

/**
 * This is the model class for table "recibos".
 *
 * @property int $id
 * @property int $matricula
 * @property string $emision
 * @property int $mes
 * @property int $año
 * @property int $estado
 * @property int $reducido
 * @property double $importe
 *
 * @property Matriculas $matricula0
 */
class Recibos extends \yii\db\ActiveRecord
{
   
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'recibos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['matricula', 'mes', 'año', 'estado', 'reducido'], 'integer'],
            [['emision'], 'safe'],
            [['importe'], 'number'],
            [['matricula'], 'exist', 'skipOnError' => true, 'targetClass' => Matriculas::className(), 'targetAttribute' => ['matricula' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'matricula' => 'Matricula',
            'emision' => 'Emision',
            'mes' => 'Mes',
            'año' => 'Año',
            'estado' => 'Estado',
            'reducido' => 'Reducido',
            'importe' => 'Importe',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMatricula0()
    {
        return $this->hasOne(Matriculas::className(), ['id' => 'matricula']);
    }
   
    
    public function getCurso0(){
      return $this->hasOne(Cursos::className(), ['id' => 'curso'])
                      ->viaTable('clases', ['id'=>'clase'])
                      ->viaTable('matriculas',['id'=>'matricula']);
        
    }
    
    public function getCurso($recibo){
        $sql = new SqlDataProvider([
            'sql' => "SELECT c.curso curso FROM(
                SELECT c1.alumno,c1.clase,curso,asignatura FROM(
                SELECT alumno,clase  FROM matriculas JOIN recibos ON matriculas.id = matricula where recibos.id =$recibo)c1 JOIN clases c ON c1.clase = c.id
              )c2 JOIN cursos c ON c.id = c2.curso",        
        ]);
       return $sql->getModels();
    }
    
    public function getAsignatura($recibo){
        $sql = new SqlDataProvider([
            'sql' => "SELECT c.asignatura FROM(
                SELECT c1.alumno,c1.clase,curso,asignatura FROM(
                SELECT alumno,clase  FROM matriculas JOIN recibos ON matriculas.id = matricula where recibos.id =$recibo)c1 JOIN clases c ON c1.clase = c.id
              )c2 JOIN asignaturas c ON c.id = c2.asignatura",        
        ]);
       return $sql->getModels();
        
    }
     public function getAlumno0()
    {
        return $this->hasOne(Alumnos::className(), ['id' => 'alumno'])
                            ->viaTable('matriculas', ['id' => 'matricula']);
        
         
    }
    
    
     public function afterFind() {
        parent::afterFind();
        $this->emision=Yii::$app->formatter->asDate($this->emision, 'php:d-m-Y');
        if($this->emision != Null){
            $this->emision=Yii::$app->formatter->asDate($this->emision, 'php:d-m-Y');
        }else{
            $this->emision = " ";
        }    
        
    }

    
    public function beforeSave($insert) {
          parent::beforeSave($insert);
          $this->emision=Yii::$app->formatter->asDate($this->emision, 'php:Y-m-d');
          $this->emision=Yii::$app->formatter->asDate($this->emision, 'php:Y-m-d');
          
          //$this->alta= \DateTime::createFromFormat("d/m/Y", $this->alta)->format("Y/m/d");
          return true;
    }
}
