<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Recibos;

/**
 * RecibosSearch represents the model behind the search form of `app\models\Recibos`.
 */
class RecibosSearch extends Recibos
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'matricula', 'mes', 'año', 'estado', 'reducido'], 'integer'],
            [['emision'], 'safe'],
            [['importe'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Recibos::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'matricula' => $this->matricula,
            'emision' => $this->emision,
            'mes' => $this->mes,
            'año' => $this->año,
            'estado' => $this->estado,
            'reducido' => $this->reducido,
            'importe' => $this->importe,
        ]);

        return $dataProvider;
    }
}
