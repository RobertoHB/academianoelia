<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use app\models\Alumnos;
use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MaterialSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Materiales';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="material-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Material', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           //'id',
           // 'fecha',
            [
            'attribute' => 'fecha',
            'value' => 'fecha',
            'format'=>'raw',
            'headerOptions' => ['style' => 'width:175px;'],
            'filter' => DatePicker::widget([
               'model'=>$searchModel,
               'attribute'=>'fecha',
                'inline' =>false,
                'clientOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd',
                ]
            ]),
                
            ],
            
            //'alumno',
            [
              'label' => 'Nombre',
              'attribute' => 'alumno',
               'value' => 'alumno0.nombre',
                'filter' => ArrayHelper::map(Alumnos::find()->all(), 'id', 'nombre','apellidos'),
              'enableSorting' => true,
            ],
             [
              'label' => 'Apellidos',
              'attribute' => 'alumno',
               'value' => 'alumno0.apellidos',
                'filter' => ArrayHelper::map(Alumnos::find()->all(), 'id', 'nombre','apellidos'),
              'enableSorting' => true,
            ],
            
            'concepto',
            'importe',

            [
             'class' => 'yii\grid\ActionColumn',
             'contentOptions'=>['style'=>'width: 70px;'],
                ],
        ],
    ]); ?>


</div>
