<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Recibos */
echo Html::button('Volver', array(
            'name' => 'btnBack',
            'class' => 'uibutton loading confirm',
            'style' => 'width:100px;',
            'onclick' => "history.go(-1)",
                )
        );
//  $this->title = 'Nuevo Recibo';
$this->params['breadcrumbs'][] = ['label' => 'Recibos', 'url' => ['index']] ;
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="recibos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
      
    ]) ?>

</div>
