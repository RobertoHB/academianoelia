<?php

use yii\helpers\Html;
use app\models\Recibos;
/* @var $this yii\web\View */
/* @var $model app\models\Recibos */

$this->title = 'Actualizar Recibo: ' . $model->id;
//$this->params['breadcrumbs'][] = ['label' => 'Recibos', 'url' =>Yii::$app->request->referrer ?: Yii::$app->homeUrl];



 
         
echo Html::button('Volver', array(
            'name' => 'btnBack',
            'class' => 'uibutton loading confirm',
            'style' => 'width:100px;',
            'onclick' => 'history.go(-1)',
//            'data-method' => 'GET',
//            'data-params' => ["recibo" =>$modelRecibos->mesRecibo
////                         "anyoRecibo" =>Yii::$app->request->post('anyoRecibo', null),
////                         "fechaEmision" =>Yii::$app->request->post('fechaEmision', null)
//                         ],
                )
        );





//$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="recibos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_formEmitidos', [
        'model' => $model,
    ]) ?>

</div>
