<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Centros */

$this->title = 'Crear Centros';
$this->params['breadcrumbs'][] = ['label' => 'Centros', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="centros-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
