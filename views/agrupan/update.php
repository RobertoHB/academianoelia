<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Agrupan */

$this->title = 'Update Agrupan: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Agrupans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="agrupan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
