<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Alumnos;

/* @var $this yii\web\View */
/* @var $model app\models\Agrupan */
/* @var $form yii\widgets\ActiveForm */

$idalumno = $_GET['id'];
$nombrealumno = $_GET['nombre'];
$apellidosalumno = $_GET['apellidos'];
$items = ArrayHelper::map(Alumnos::find()->all(), 'id','nombre','apellidos');
?>

<div class="agrupan-form">

    <?php $form = ActiveForm::begin(); ?>

    <!--// $form->field($model, 'alumno')->textInput()--> 
    <?= $form->field($model, 'alumno')->hiddenInput(['value' => $idalumno])->label(false);?>

    <!--$form->field($model, 'alumno_grupo')->textInput()--> 
    <?= $form->field($model, 'alumno_grupo') ->dropDownList(
          $items,           // Flat array ('id'=>'label')
          ['prompt'=>'']);      // options; ?>

    <?= $form->field($model, 'observaciones')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
