<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Matriculas */

$this->title = 'Crear Matricula';
$this->params['breadcrumbs'][] = ['label' => 'Matricula/Alumno', 'url' =>Yii::$app->request->referrer ?: Yii::$app->homeUrl];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="matriculas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'itemClases' =>$itemClases,
        'itemAlumnos' => $itemAlumnos,
    ]) ?>

</div>
