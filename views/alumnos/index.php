<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AlumnosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Alumnos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="alumnos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Alumnos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nombre',
            'apellidos',
            'dni',
            'direccion',
            //'poblacion',
            //'movil',
            //'fijo',
            //'email:email',
            //'alta',
            //'curso',
            //'centro',
            //'observaciones:ntext',

            ['class' => 'yii\grid\ActionColumn',
                 'contentOptions'=>['style'=>'width: 70px;'],
                
            ],
            
               [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{agrupaciones}',
                'contentOptions'=>['style'=>'width: 30px;'],
                'buttons' => [
                'agrupaciones' => function ($url, $model) {
                                    return Html::a('<span class="glyphicon glyphicon-user"></span>', $url, [
                                        'title' => Yii::t('app', 'Agrupan'),
                                    ]);

                                }, 
                                
                ],
                        
                'urlCreator' => function ($action, $model, $key, $index) {
                     return Url::to(['agrupan/index', 'id' => $model->id,'nombre'=>$model->nombre,'apellidos'=>$model->apellidos]);
                }        
                 
            ],    
            [
              'class' => 'yii\grid\ActionColumn',
              'template' => '{matriculas}',
              'contentOptions'=>['style'=>'width: 40px;'],
              'buttons' => [
              'matriculas' => function ($url, $model) {
                                  return Html::a('<span class="glyphicon glyphicon-bookmark"></span>', $url, [
                                      'title' => Yii::t('app', 'Matriculas'),
                                  ]);

                              }, 

                             ],

              'urlCreator' => function ($action, $model, $key, $index) {
                   return Url::to(['matriculas/matriculas_alumno', 'alumno' => $model->id,'nombre'=>$model->nombre,'apellidos'=>$model->apellidos]);
              }        

            ],          
            [
              'class' => 'yii\grid\ActionColumn',
              'template' => '{duplicados}',
              'contentOptions'=>['style'=>'width: 40px;'],
              'buttons' => [
              'duplicados' => function ($url, $model) {
                                  return Html::a('<span class="glyphicon glyphicon-duplicate"></span>', $url, [
                                      'title' => Yii::t('app', 'Duplicar Alumno'),
                                  ]);

                              }, 

                             ],

              'urlCreator' => function ($action, $model, $key, $index) {
                   return Url::to(['alumnos/duplicar', 'alumno' => $model->id]);
              }        

            ],          
            
            
 
        ],
    ]); ?>


</div>
