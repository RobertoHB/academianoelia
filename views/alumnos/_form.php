<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DatePicker;
use yii\helpers\ArrayHelper;
use app\models\Centros;
use app\models\Cursos;

/* @var $this yii\web\View */
/* @var $model app\models\Alumnos */
/* @var $form yii\widgets\ActiveForm */

$ListCentros = ArrayHelper::map(Centros::find()->all(), 'id', 'nombre');
$listaCursos = ArrayHelper::map(Cursos::find()->all(), 'id', 'curso');
$formasPago = [1 => 'Efectivo', 2 => 'Transferencia']; 
?>

<div class="alumnos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'apellidos')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dni')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'direccion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'poblacion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'movil')->textInput() ?>

    <?= $form->field($model, 'fijo')->textInput() ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

     <!--$form->field($model, 'alta')->textInput()-->
     <!--$form->field($model,'alta')->widget(DatePicker::className(),['clientOptions' => ['dateFormat' => 'd-m-yyyy']]) ?>-->
    <?= $form->field($model, 'alta')->widget(DatePicker::className(), [
            // inline too, not bad
            'inline' => false, 
             // modify template for custom rendering
            //'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
            'clientOptions' => [
            'autoclose' => true,
            'format' => 'dd-mm-yyyy',
            'todayBtn' => true
            ]
    ]);?>

     
    <?= $form->field($model, 'curso')->dropDownList($listaCursos, ['prompt' => 'Seleccione Uno' ],['maxlength'=>true]); ?>
    <?= $form->field($model, 'centro')->dropDownList($ListCentros, ['prompt' => 'Seleccione Uno' ]); ?>
    <?= $form->field($model, 'pago')->dropDownList($formasPago, ['prompt' => 'Seleccione Uno' ]); ?>
    <?= $form->field($model, 'observaciones')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
       
    </div>
    
       
    
    <?php ActiveForm::end(); ?>

</div>
