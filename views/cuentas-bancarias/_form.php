<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Cuentas_bancarias */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cuentas-bancarias-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'alumno')->textInput() ?>

    <?= $form->field($model, 'iban')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'entidad')->textInput() ?>

    <?= $form->field($model, 'oficina')->textInput() ?>

    <?= $form->field($model, 'dc')->textInput() ?>

    <?= $form->field($model, 'cuenta')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
