﻿DROP DATABASE IF EXISTS academia_noelia;
CREATE DATABASE academia_noelia
  CHARACTER SET UTF8
  COLLATE utf8_general_ci;
USE academia_noelia;

DROP TABLE IF EXISTS alumnos;
CREATE OR REPLACE TABLE alumnos(
  id int(11) AUTO_INCREMENT,
  nombre varchar(100),
  apellidos varchar(200),
  dni varchar(9),
  direccion varchar(200),
  poblacion varchar(100),
  movil int(11),
  fijo int(11),
  email varchar(200),
  alta date,
  curso varchar(50),
  centro varchar(100),
  observaciones text,
  PRIMARY KEY(id),
  UNIQUE(dni)
);

DROP TABLE IF EXISTS asignaturas;
CREATE OR REPLACE TABLE asignaturas(
  id int(11) AUTO_INCREMENT,
  asignatura varchar(50),
  PRIMARY KEY(id),
  UNIQUE(asignatura)
);

DROP TABLE IF EXISTS cursos;
CREATE OR REPLACE TABLE cursos(
  id int(11) AUTO_INCREMENT,
  curso varchar(50),
  PRIMARY KEY(id),
  UNIQUE(curso)
);

DROP TABLE IF EXISTS profesores;
CREATE OR REPLACE TABLE profesores(
  id int(11) AUTO_INCREMENT,
  nombre varchar(100),
  apellidos varchar(200),
  direccion varchar(200),
  poblacion varchar(100),
  dni varchar(9),
  movil int(11),
  fijo int(11),
  email varchar(200),
  alta date,
  baja date,
  observaciones text,
  PRIMARY KEY(id),
  UNIQUE(dni)
);

DROP TABLE IF EXISTS imparten;
CREATE OR REPLACE TABLE imparten(
  id int(11) AUTO_INCREMENT,
  profesor int(11),
  asignatura int(11),
  -- CONSTRAINT PK_imparten PRIMARY KEY(profesor,asignatura),
  PRIMARY KEY(id),
  UNIQUE(profesor,asignatura), 
  CONSTRAINT imparten_profesores FOREIGN KEY (profesor) REFERENCES profesores(id),
  CONSTRAINT imparten_asignaturas FOREIGN KEY(asignatura) REFERENCES asignaturas(id)
);


DROP TABLE IF EXISTS clases;
CREATE OR REPLACE TABLE clases(
  id int(11) AUTO_INCREMENT,
  curso int(11),
  asignatura int(11),
  importe float(3,2),
  observaciones text,
  -- CONSTRAINT PK_clases PRIMARY KEY(curso,asignatura),
  PRIMARY KEY(id),
  UNIQUE(curso,asignatura),
  CONSTRAINT clases_curso FOREIGN KEY (curso) REFERENCES cursos(id),
  CONSTRAINT clases_asignatura FOREIGN KEY (asignatura) REFERENCES asignaturas(id)
);


DROP TABLE IF EXISTS matriculas;
CREATE OR REPLACE TABLE matriculas(
  id int(11) AUTO_INCREMENT,
  alumno int(11),
  clase int(11),
  alta date,
  baja date,
  observaciones text,
 -- CONSTRAINT PK_matriculas PRIMARY KEY(alumno,clase,alta),
  PRIMARY KEY(id),
  UNIQUE(alumno,clase,alta),
  CONSTRAINT matriculan_alumnos FOREIGN KEY(alumno) REFERENCES alumnos(id),
  CONSTRAINT matriculan_clases FOREIGN KEY(clase) REFERENCES clases(id)
);


DROP TABLE IF EXISTS agrupan;
CREATE OR REPLACE TABLE agrupan(
  id int(11) AUTO_INCREMENT,
  alumno int(11),
  alumno_grupo int(11),
  observaciones text,
  PRIMARY KEY(id),
  UNIQUE(alumno,alumno_grupo),
  CONSTRAINT agrupan_alumnos FOREIGN KEY(alumno) REFERENCES alumnos(id)
);


DROP TABLE IF EXISTS cuentas_bancarias;
CREATE OR REPLACE TABLE cuentas_bancarias(
  id int(11) AUTO_INCREMENT,
  alumno int(11),
  iban varchar(4),
  entidad int(4),
  oficina int(4),
  dc int(2),
  cuenta int(10),
  PRIMARY KEY(id),
  CONSTRAINT cuentasbancarias_alumnos FOREIGN KEY(alumno) REFERENCES alumnos(id)
);

DROP TABLE IF EXISTS recibos;
CREATE OR REPLACE TABLE recibos(
  id int(11) AUTO_INCREMENT,
  matricula int(11),
  emision date,
  mes int(2),
  año int(4),
  estado tinyint,
  reducido int(3),
  importe float(4,2),
  PRIMARY KEY(id),
  CONSTRAINT recibos_matriculas FOREIGN KEY(matricula) REFERENCES matriculas(id)
);




-- Insertando registros en alumnos

  INSERT INTO alumnos VALUES('','Felipe','Fernandez Perez','13695248A','Barrio Pronillo 52, 4A','Solares','668584236','','perezfernandezfelipe@gmail.com','2019-09-01','5ESO','Apostolado Ceceñas','');
  INSERT INTO alumnos VALUES('','Arturo','Vazquez Lopez','18695248G','Calvo Sotelo 21, 6B','Solares','668584587','','vazquezlopezarturo@gmail.com','2019-06-15','6ESO','Apostolado Ceceñas','');
  INSERT INTO alumnos VALUES('','Maria','Sainz Solano','20185231F','Barrio San Pedro 15','Solares','675589125','','ssolano@gmail.com','2019-01-10','4ESO','C.P.Marques de Valdecilla','');
  INSERT INTO alumnos VALUES('','Sandra','Gomez Martinez','19458124R','Bajada de la Iglesia, 5','Solares','655548187','','gmartinez@gmail.com','2019-01-12','6ESO','C.P.Marques de Valdecilla','');
  INSERT INTO alumnos VALUES('','Alexandra','Perez del Molino','21548786W','C/Cuatro caminos,25 ,5B','Solares','589889745','','pdelmolino@gmail.com','2019-02-09','7ESO','C.P.Marques de Valdecilla','');

  SELECT * FROM alumnos a;
-- Insertando registros en asignaturas

  INSERT INTO asignaturas VALUES('','Matematicas');
  INSERT INTO asignaturas VALUES('','Lengua');
  INSERT INTO asignaturas VALUES('','Fisica');
  INSERT INTO asignaturas VALUES('','Ciencias');
  INSERT INTO asignaturas VALUES('','General');

 SELECT * FROM asignaturas a;

-- Insertando registros en cursos

   INSERT INTO cursos VALUES('','1PRI');
   INSERT INTO cursos VALUES('','2PRI');
   INSERT INTO cursos VALUES('','3ESO');
   INSERT INTO cursos VALUES('','5ESO');
   INSERT INTO cursos VALUES('','1BACHI');

  SELECT * FROM cursos c; 

-- Insertando registros en profesores

INSERT INTO profesores VALUES('','Federico','Garcia Sanchez','C/Burgos, 16,5A','Santander','20195897A','658978254','942375896','gsanchez@gmail.com','2015-10-01','','');
INSERT INTO profesores VALUES('','Marcos','Lopez Dominguez','C/Calvo Sotelo,22,7B','Solares','20458458E','648897254','','glopez@gmail.com','2010-01-20','','');
INSERT INTO profesores VALUES('','Zulema','Martinez De la Torre','C/vargas, 36,3A','Santander','21258148T','655897147','','zmartinezez@gmail.com','2017-09-13','','');
INSERT INTO profesores VALUES('','Maria','Gomez Sanchez','Barrio San Pedro, 1','Solares','22457963B','787521699','','msanchez@gmail.com','2018-09-15','','');
INSERT INTO profesores VALUES('','Andrea','Alvarez Solar','C/cuatro caminos, 10,5A','Solares','23254125E','657255458','','asolar@gmail.com','2016-11-22','','');

SELECT * FROM profesores p;

-- Insertado registros en imparten


  INSERT INTO imparten VALUES('','1','1');
  INSERT INTO imparten VALUES('','1','2');
  INSERT INTO imparten VALUES('','1','3');
  INSERT INTO imparten VALUES('','2','1');
  INSERT INTO imparten VALUES('','2','2');
  INSERT INTO imparten VALUES('','3','1');
  INSERT INTO imparten VALUES('','3','4');
  INSERT INTO imparten VALUES('','4','4');
  INSERT INTO imparten VALUES('','5','5');

SELECT * FROM imparten JOIN profesores ON profesor= profesores.id JOIN asignaturas ON imparten.asignatura = asignaturas.id;



-- Insertando registros en clases



INSERT INTO clases VALUES('','1','1','50','Todos los dias de la semana');
INSERT INTO clases VALUES('','1','2','50','Todos los dias de la semana');
INSERT INTO clases VALUES('','2','2','50','Todos los dias de la semana');
INSERT INTO clases VALUES('','3','4','50','Lunes,miercoles,viernes');
INSERT INTO clases VALUES('','4','5','50','Todos los dias de la semana');
INSERT INTO clases VALUES('','5','1','50','Todos los dias de la semana');


SELECT * FROM clases;
UPDATE clases SET importe = 50 WHERE importe <>50;

-- Insertando registros en matriculas


  INSERT INTO matriculas VALUES('','1','2','2019-10-01','','');
  INSERT INTO matriculas VALUES('','2','1','2018-10-01','','');
  INSERT INTO matriculas VALUES('','2','2','2019-10-01','','');
  INSERT INTO matriculas VALUES('','3','4','2019-10-20','','');
  INSERT INTO matriculas VALUES('','4','5','2019-10-15','','');
  INSERT INTO matriculas VALUES('','5','2','2018-10-09','','');
  INSERT INTO matriculas VALUES('','5','2','2019-10-09','','');


  SELECT * FROM matriculas JOIN alumnos ON alumnos.id = alumno JOIN clases ON clases.id = clase;

  -- Insertando registros en Agrupan


    INSERT INTO agrupan VALUES('','1','5','Hermanos. Se agrupan los recibos');

        SELECT * FROM alumnos al JOIN(agrupan) ON al.id=alumno;

-- Insertando registros en





  





  

ALTER TABLE cursos ADD COLUMN IF NOT EXISTS
    descripcion varchar(300);

ALTER TABLE clases
    MODIFY COLUMN IF EXISTS
     importe float;

ALTER TABLE alumnos
    MODIFY COLUMN IF EXISTS
      centro int(11);

ALTER TABLE alumnos
    ADD CONSTRAINT alumnos_centros FOREIGN KEY (centro) 
      REFERENCES centros(id);
   

/*
ALTER TABLE imparten ADD PRIMARY KEY(profesor,asignatura);
ALTER TABLE clases ADD PRIMARY KEY(curso,asignatura);

ALTER TABLE matriculas ADD PRIMARY KEY(alumno,clase,alta);

ALTER TABLE agrupan ADD PRIMARY KEY(alumno,alumno_grupo);
*/

USE academia_noelia;

CREATE OR REPLACE TABLE users(
  id int(11) AUTO_INCREMENT,
  username varchar(80),
  password varchar(255),
  email varchar(255),
  authKey varchar(255),
  accessToken varchar(255),
  activate tinyint,
  PRIMARY KEY(id),
  UNIQUE(username,PASSWORD)
);


CREATE OR REPLACE TABLE centros(
  id int(11) AUTO_INCREMENT,
  nombre varchar(200),
  localidad varchar(100),
  PRIMARY KEY(id),
  UNIQUE(nombre)
  
);


CREATE OR REPLACE TABLE material(
  id int(11) AUTO_INCREMENT,
  fecha date,
  alumno int(11),
  concepto varchar(100),
  importe float(4,2),
  PRIMARY KEY(id),
  CONSTRAINT material_alumno FOREIGN KEY(alumno) REFERENCES alumnos(id)
);


ALTER TABLE alumnos ADD COLUMN IF NOT EXISTS
    pago varchar(50);


